# Locate
## Description
the plugin permit to locate on the canvas coordinnates 
who use the projection WGS84 EPSG:4326 in decimal or sexagecimal format (Degrees, Minutes, Seconds).
It's very important to respect the format below.

Examples:

* Decimal: lat= 47.209991 and long=-1.744007

* Sexagecimal: lat= 47°12'35.9676"N and long 1°44'38.4252"W

## Installation

Go to the extension menu on QGis and research "Locate". After that click on "Installation", that's it you will see the logo in your toolbar.

## Technology used

* Python 3
* PyQGis
* QtDesigner
* Regex

