"""
    Module contains the methods for coordinates
"""

import re
from qgis.core import (
    QgsRectangle,
    QgsPoint,
    QgsCoordinateReferenceSystem,
    QgsCoordinateTransform,
    QgsProject,
    QgsGeometry,
)

class Coordinates:
    """Management of the coordinates """

    def __init__(self, iface, canvas):
        self.canvas = canvas
        self.iface = iface
        self.deci_latitude = 0.0
        self.deci_longitude = 0.0
        self.scale = 0.0003
        self.epsg_project = ""
        self.epsg_source = ""
        self.epsg_destination = ""
        self.epsg_transformation = ""
        self.orientation_latitude = ""
        self.orientation_longitude = ""
        self.pattern_sexagesimal = r"(?P<degrees>^[0-9]*)([ ]*\°[ ]*)(?P<minutes>[0-9]*)([ ]*'[ ]*)(?P<seconds>[0-9]*[.,][0-9]*)([ ]*\"[ ]*)(?P<orientation>[NSEWO])"
        self.pattern_decimal = r"(?P<sign>[-]*)(?P<integer>[0-9]*)[.,]*(?P<decimal>[0-9]*)"

    def take_epsg_from_project(self):
        """Take the EPSG from the project
        Returns:
            [object]
        """
        self.epsg_project = self.iface \
            .mapCanvas()\
            .mapSettings()\
            .destinationCrs()\
            .authid()
        return self.epsg_project

    def set_espg_source(self):
        """Set the EPSG:4326
        Returns:
            [object]
        """
        self.epsg_source = QgsCoordinateReferenceSystem(4326)
        return self.epsg_source

    def set_transformation(self):
        """Set the ESPG transformation
        Returns:
            [object] -- Transformation
        """
        source = self.epsg_source
        destination = self.epsg_destination
        self.epsg_transformation = QgsCoordinateTransform(
            source, destination, QgsProject.instance())
        return self.epsg_transformation

    def set_epsg_destination(self):
        """Set the EPSG destination
        Returns:
            [object] -- EPSG of the project
        """
        self.epsg_destination = QgsCoordinateReferenceSystem(self.epsg_project)
        return self.epsg_destination

    def remove_comma(self, input_car):
        """The function remove the comma of a string
        Arguments:
            input_car {string}
        Returns:
            [string] -- without comma
        """
        car_without_comma = input_car.replace(",", ".")
        return car_without_comma

    def check_if_float(self, input_car):
        """Check if the input is a flaot
        Arguments:
            input_car {string}
        Returns:
            [boolean]
        """
        input_car = str(input_car)
        
        if re.match(self.pattern_decimal, input_car) is not None:
            try:
                float(input_car)            
                flag = True
            except:
                flag = False
        else:
            flag = False
        return flag

    def check_if_sexagesimal(self, input_car):
        """Check if the input respect the sexagesimal format
        Arguments:
            input_car {string}
        Returns:
            [boolean]
        """
        input_car = str(input_car)

        if re.match(self.pattern_sexagesimal, input_car) is not None:
            flag = True
        else:
            flag = False
        return flag

    def set_orientation_latitude(self):
        """Return the orientation of the latitude]
        Returns:
            [string] -- N or S
        """
        self.orientation_latitude = ''

        if float(self.deci_latitude) >= 0:
            self.orientation_latitude = 'N'
        else:
            self.orientation_latitude = 'S'
        return self.orientation_latitude

    def set_orientation_longitude(self):
        """Return the orientation of the longitude]
        Returns:
            [string] -- E or W
        """
        self.orientation_longitude = ''

        if float(self.deci_longitude) >= 0:
            self.orientation_longitude = 'E'
        else:
            self.orientation_longitude = 'W'
        return self.orientation_longitude

    def set_unsigned_value(self, value):
        """The function permit to unsigned a float
        Arguments:
            value {float}
        Returns:
            [str] -- with unsigned float
        """
        if float(value) < 0:
            unsigned_value = str(float(value) * -1)
        else:
            unsigned_value = value
        return str(unsigned_value)

    def format_sexagesimal(self, sexagesimal_str):
        """
        Take a string and return a list
        [degrees, minutes, seconds, orientation]
        """
        regex = re.match(self.pattern_sexagesimal, sexagesimal_str)
        degrees = int(regex.group('degrees'))
        minutes = int(regex.group('minutes'))
        seconds = float(regex.group('seconds'))
        orientation = str(regex.group('orientation'))

        return [degrees, minutes, seconds, orientation]

    def convert_decimal_to_sexagesimal(self, decimal, orientation):
        """Function return a string with the elements below
        degrees, minutes, seconds, orientation
        Arguments:
            decimal {float}
            orientation {string} -- N, S, W, E
        Returns:
            [string] -- degrees, minutes, seconds, orientation
        """

        degrees = 0.0
        minutes = 0.0
        seconds = 0.0

        indice_degrees = 0
        for car in decimal:
            if car == '.':
                degrees = int(decimal[0:indice_degrees])
                break
            indice_degrees += 1

        convert_to_minutes = round((float(decimal) - degrees) * 60, 6)

        indice_minutes = 0
        for car in str(convert_to_minutes):

            if car == '.':
                minutes = str(convert_to_minutes)[0:indice_minutes]
                break
            indice_minutes += 1

        seconds = round((float(convert_to_minutes) - float(minutes)) * 60, 6)
        sexagesimal = "{}°{}\'{}\"{}".format(
            degrees, minutes, seconds, orientation)
        return sexagesimal

    def convert_sexagesimal_to_decimal(self, sexagesimal):
        """Convert a sexagesimal string into decimal
        Arguments:
            sexagesimal {string} -- with arguments below
            degrees, minutes, seconds, orientation
        Returns:
            [float] -- Float
        """
        degrees = sexagesimal[0]
        minutes = sexagesimal[1]
        seconds = sexagesimal[2]
        orientation = sexagesimal[3]

        if orientation in ('W', 'O', 'S'):
            decimal = (degrees * -1) - (minutes / 60) - (seconds / 3600)
        else:
            decimal = degrees + (minutes / 60) + (seconds / 3600)

        return round(decimal, 6)

    def zoom_to_canvas(self):
        """The function permit to zoom on the canvas.
        """
        rectangle = self.epsg_transformation.transform(QgsRectangle(
            self.deci_longitude - self.scale,
            self.deci_latitude - self.scale,
            self.deci_longitude + self.scale,
            self.deci_latitude + self.scale,
        ))
        self.canvas.setExtent(rectangle)
        self.canvas.refresh()

    def transform_epsg(self):
        """Funtion permit to transfor the epsg
        From source to destination
        Returns:
            Object -- point with coordinates
        """

        point_epsg_project = QgsGeometry(
            QgsPoint(self.deci_longitude, self.deci_latitude))
        point_epsg_project.transform(self.epsg_transformation)

        point = point_epsg_project.asPoint()
        return point

